﻿namespace HeroesFs.Controllers

open System
open System.Collections.Generic
open System.Linq
open System.Threading.Tasks
open Microsoft.AspNetCore.Mvc
open Microsoft.Extensions.Logging

open HeroesFs
open HeroesFs.Types


[<ApiController>]
[<Route("[controller]")>]
type HeroesController(logger: ILogger<HeroesController>) as this =
    inherit ControllerBase()

    [<HttpGet>]
    member _.Get(): Async<list<Hero>> = async { return Hero.find () }

    [<HttpGet("{id:int}")>]
    member _.GetOne(id: int): Async<IActionResult> =
        async {
            match Hero.findOne id with
            | Some hero -> return this.Ok(hero) :> IActionResult
            | None ->
                logger.LogInformation(sprintf "Requested User with Id {%i} Not Found" id)
                this.Response.StatusCode <- 404
                return this.NotFound() :> IActionResult
        }

    [<HttpPost>]
    member _.Post(hero: HeroPayload): Async<Hero> =
        async {
            this.Response.StatusCode <- 201
            return Hero.create hero.name hero.powers
        }

    [<HttpPut("{id:int}")>]
    member _.Put(id: int, hero: Hero): Async<Hero> = async { return Hero.update id hero }

    [<HttpDelete("{id:int}")>]
    member _.Delete(id: int): Async<int> = async { return Hero.delete id }
